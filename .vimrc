syntax on
set nu
set autoindent
set ts=4
set expandtab
set showmatch
set encoding=utf-8

" copy & paste
set pastetoggle=<F2>
set clipboard=unnamed

" show the filepath
set statusline+=%F
set laststatus=2

" window split preference
set splitbelow splitright

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" adjust split screen sizes
nnoremap <silent> <C-Left> :vertical resize +3<CR>
nnoremap <silent> <C-Right> :vertical resize -3<CR>
nnoremap <silent> <C-Up> :resize -3<CR>
nnoremap <silent> <C-Down> :resize +3<CR>

" toggle two split windows from horizontal to vertical (and vice versa)
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" highlight when over 80 characters
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%80v', 100)

" auto detect indent after colon
filetype plugin indent on

" turn off jedi docstring popup
autocmd FileType python setlocal completeopt-=preview

" show redundant whitespace
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$/

" wrap text in *.txt files
au BufRead,BufNewFile *.txt setlocal textwidth=80

" remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e
